using System;

namespace DatingApp.API.Models
{
    public class Publicacion
    {
        public int Id { get; set; }
        public string Puesto { get; set; }
        public string Salario { get; set; }
       public string User { get; set; }
        public string Localizacion { get; set; }

        public string Descripcion { get; set; }
    
        public string  Jornada { get; set; }
        public string Empresa { get; set; }
        public string Contacto { get; set; }

        public string Carrera { get; set; }

        public DateTime fecha {get; set;}

        public Boolean Aprobado { get; set; }

        }
}