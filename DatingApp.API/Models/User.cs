namespace DatingApp.API.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email  { get; set; }

        public string Role {get; set; }
        
        public Cv Cv {get; set;}

        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
    }

    public class Cv {
        public int Id {get; set;}
        public string FullName { get; set; }
        public string FullRoot { get; set; }
    }
}