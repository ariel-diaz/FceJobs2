using System;

namespace DatingApp.API.Models
{
    public class Postulacion
    {
        public int Id { get; set; }
        public string User { get; set; }
        public string IdPublicacion { get; set; }

        public bool Status { get; set; }
        public DateTime Fecha { get; set; }
    }
}