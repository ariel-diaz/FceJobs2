using System.Linq;
using System.Threading.Tasks;
using DatingApp.API.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.API.Controllers
{  
    

    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly DataContext _context;
        public UserController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string user) {
            var usernames = await _context.Users.Include(x => x.Cv).ToListAsync();
            return Ok(usernames);
        } 

        [HttpGet("{user}")]
        public async Task<IActionResult> GetUser(string user) {
            var username = await _context.Users.Include(x => x.Cv).FirstOrDefaultAsync(x => x.Username == user);
            return Ok(username);
        } 
    }
}