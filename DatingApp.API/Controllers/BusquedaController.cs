using System.Linq;
using System.Threading.Tasks;
using DatingApp.API.Data;
using DatingApp.API.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.API.Controllers
{

    [Route("api/[controller]")]
    public class BusquedaController : Controller
    {
        private readonly DataContext _context;
        private readonly IDatingRepository _repo;
        public BusquedaController(DataContext context, IDatingRepository repo)
        {
            _context = context;
            _repo = repo;

        }


        [HttpGet("{busqueda}")]
        public async Task<IActionResult> BuscarPublicaciones(string busqueda, [FromQuery]PublicacionParams pubsParam)
        {
            var pubs = await _repo.BusquedaPublicaciones(busqueda, pubsParam);
            Response.AddPagination(pubs.CurrentPage, pubs.PageSize, pubs.TotalCount, pubs.TotalPages);
            return Ok(pubs);
        }

    }
}