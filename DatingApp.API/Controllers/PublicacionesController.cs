using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using DatingApp.API.Data;
using DatingApp.API.Dtos;
using DatingApp.API.Helpers;
using DatingApp.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.API.Controllers
{

    [Route("api/[controller]")]
    public class PublicacionesController : Controller
    {

        private readonly DataContext _context;
        private readonly IDatingRepository _repo;

        public PublicacionesController(DataContext context, IDatingRepository repo)
        {
            _context = context;
            _repo = repo;
        }


        // GET api/values
        [HttpGet]
        public async Task<IActionResult> GetPublicaciones([FromQuery]PublicacionParams pubsParam)
        {

            var pubs = await _repo.GetPublicaciones(pubsParam);
            Response.AddPagination(pubs.CurrentPage, pubs.PageSize, pubs.TotalCount, pubs.TotalPages);
            return Ok(pubs);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPublicacion(int id)
        {
            var publicacion = await _context.Publicaciones.FirstOrDefaultAsync(x => x.Id == id);
            return Ok(publicacion);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PublicacionDto pubDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (pubDto == null)
            {
                return BadRequest("El cuerpo esta vacio");
            }

            var pub = new Publicacion
            {
                Puesto = pubDto.Puesto,
                Salario = pubDto.Salario,
                User = pubDto.User,
                Localizacion = pubDto.Localizacion,
                Descripcion = pubDto.Descripcion,
                Jornada = pubDto.Jornada,
                Empresa = pubDto.Empresa,
                Contacto = pubDto.Contacto,
                fecha = pubDto.Fecha,
                Aprobado = false,
                Carrera = pubDto.Carrera
            };


            await _context.Publicaciones.AddAsync(pub);
            _context.SaveChanges();

            return StatusCode(201);

        }

        [HttpGet("panel/{user}")]
        public async Task<IActionResult> getPublicacionesPorUsuario(string user)
        {

            if (string.IsNullOrEmpty(user))
            {
                return BadRequest("Error en el usuario");
            }
            var publicaciones = await _context.Publicaciones.Where(x => x.User == user).ToListAsync();
            return Ok(publicaciones);
        }




        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id)
        {
            var publicacion = await _context.Publicaciones.FirstOrDefaultAsync(x => x.Id == id);

            if (publicacion == null)
            {
                BadRequest("La publicacion no existe");
            }

            publicacion.Aprobado = !publicacion.Aprobado;

            _context.Update(publicacion);
            _context.SaveChanges();
            return StatusCode(201);


        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }


    }
}
