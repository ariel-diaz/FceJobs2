using System;
using System.Linq;
using System.Threading.Tasks;
using DatingApp.API.Data;
using DatingApp.API.Dtos;
using DatingApp.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.API.Controllers
{
    [Route("api/[controller]")]
    public class PostulacionesController : Controller
    {

        private readonly DataContext _context;

        public PostulacionesController(DataContext context)
        {
            _context = context;
        }

        // GET

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var postulaciones = await _context.Postulaciones.ToListAsync();
            return Ok(postulaciones);
        }


         [HttpGet("{id}")]
        public async Task<IActionResult> GetSpecificPostulaciones(string id)
        {
            var postulaciones = await _context.Postulaciones.Where(x => x.IdPublicacion == id).ToListAsync();
            return Ok(postulaciones);
        }


        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PostulacionDto postDto)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (postDto == null)
            {
                return BadRequest("El cuerpo esta vacio");
            }

            var user = await _context.Postulaciones.FirstOrDefaultAsync(x => (x.User == postDto.User && x.IdPublicacion == postDto.IdPublicacion));

            if (user != null)
            {
                return BadRequest("El usuario ya se postulo");
            }

            var post = new Postulacion
            {
                User = postDto.User,
                IdPublicacion = postDto.IdPublicacion,
                Status = false,
                Fecha = new DateTime()
            };

            await _context.Postulaciones.AddAsync(post);
            _context.SaveChanges();
            return StatusCode(201);
        }


        [HttpDelete]
        public async  Task<IActionResult> Delete([FromQuery]int idOferta, string user) {
            var oferta = await _context.Postulaciones
                            .Where(x => x.IdPublicacion == idOferta.ToString() && x.User == user)
                                .FirstOrDefaultAsync();
            if(oferta == null) {
                return NotFound();
            }

            _context.Postulaciones.Remove(oferta);
            _context.SaveChanges();

            return Ok();
        }
    }
}