using System;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using DatingApp.API.Data;
using DatingApp.API.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;

namespace DatingApp.API.Controllers
{


    [Route("api/[controller]")]
    public class UploadController : Controller
    {
        private readonly IHostingEnvironment _iHostingEnviroment;
        private readonly DataContext _context;

        public UploadController(IHostingEnvironment iHostingEnviroment, DataContext context)
        {
            _iHostingEnviroment = iHostingEnviroment;
            _context = context;
        }

        [HttpPost]
        public async Task<ActionResult> UploadFile([FromQuery]string userName)
        {
            try
            {
                var file = Request.Form.Files[0];
                var currentUser = await _context.Users.Include(x => x.Cv)
                                    .Where(x => x.Username == userName).FirstOrDefaultAsync();

                string folderName = "Cvs";
                if (string.IsNullOrWhiteSpace(_iHostingEnviroment.WebRootPath))
                {
                    _iHostingEnviroment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                }
                string newPath = Path.Combine(_iHostingEnviroment.WebRootPath, folderName);
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (file.Length > 0)
                {
                    string fileName = currentUser.Id + "_" +
                    ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    string fullPath = Path.Combine(newPath, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    currentUser.Cv.FullName = fileName;
                    currentUser.Cv.FullRoot = fullPath;

                    _context.Update(currentUser);
                    _context.SaveChanges();
                }
                return Ok("Upload Succesfull");
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}