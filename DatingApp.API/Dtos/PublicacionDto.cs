using System;
using System.ComponentModel.DataAnnotations;

namespace DatingApp.API.Dtos
{
    public class PublicacionDto
    {

        [Required]
        public string Puesto { get; set; }
        public string Salario { get; set; }
        public string User { get; set; }

        [Required]
        public string Localizacion { get; set; }
        [Required]
        public string Descripcion { get; set; }

        public string Carrera { get; set; }
        public string Jornada { get; set; }
        public string Empresa { get; set; }
        public string Contacto { get; set; }
        public DateTime Fecha {get; set;}
        public Boolean Aprobado { get; set; }
    }
}