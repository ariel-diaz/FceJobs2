using System.ComponentModel.DataAnnotations;

namespace DatingApp.API.Dtos
{
    public class UserForRegisterDto
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Email { get; set; }
        
        public string Role { get; set; }
       
        [Required]
        [StringLength(28, MinimumLength = 4, ErrorMessage = "Tu password tiene que tener entre 4 y 8 caracteres")]
        public string Password { get; set; }
    }
}