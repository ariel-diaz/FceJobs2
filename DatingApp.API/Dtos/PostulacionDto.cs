using System;
using System.ComponentModel.DataAnnotations;

namespace DatingApp.API.Dtos
{
    public class PostulacionDto
    {
        [Required]
        public string User { get; set; }
        [Required]
        public string IdPublicacion { get; set; }
        [Required]
        public bool Status { get; set; }
        
        public DateTime Fecha { get; set; }
        
    }
}