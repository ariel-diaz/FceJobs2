using System;

namespace DatingApp.API.Helpers
{
    public class PublicacionParams
    {
        private const int MaxPageSize = 50;
        public int PageNumber { get; set; } = 1;
        private int pageSize = 10;
        public int PageSize
        {
            get { return pageSize;}
            set { pageSize = (value > MaxPageSize) ? MaxPageSize : value;}
        }

        public string Jornada { get; set; }
        public string Carrera { get; set; }
        public string Fecha { get; set; }

        public Boolean OnlyOk {get; set;}     
    }
}