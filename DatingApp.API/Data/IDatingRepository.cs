using System.Threading.Tasks;
using DatingApp.API.Helpers;
using DatingApp.API.Models;

namespace DatingApp.API.Data
{
    public interface IDatingRepository
    {
        Task<PagedList<Publicacion>> GetPublicaciones(PublicacionParams pubParams);
        Task<PagedList<Publicacion>> BusquedaPublicaciones(string busqueda, PublicacionParams pubParams);
    }
}