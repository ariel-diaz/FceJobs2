using System;
using System.Linq;
using System.Threading.Tasks;
using DatingApp.API.Helpers;
using DatingApp.API.Models;

namespace DatingApp.API.Data
{
    public class DatingRepository : IDatingRepository
    {
        private readonly DataContext _context;

        public DatingRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<PagedList<Publicacion>> BusquedaPublicaciones(string busqueda, PublicacionParams pubParams)
        {
            var pubs = _context.Publicaciones.Where(x => x.Puesto.ToLower()
            .Contains(busqueda.ToLower()));
            pubs = FilterData(pubs, pubParams);
            return await PagedList<Publicacion>.CreateAsync(pubs, pubParams.PageNumber, pubParams.PageSize);
        }

        public async Task<PagedList<Publicacion>> GetPublicaciones(PublicacionParams pubParams)
        {
            var pubs = _context.Publicaciones.AsQueryable();
            pubs = FilterData(pubs, pubParams);
            return await PagedList<Publicacion>.CreateAsync(pubs, pubParams.PageNumber, pubParams.PageSize);
        }


        public IQueryable<Publicacion> FilterData(IQueryable<Publicacion> pubs, PublicacionParams pubParams)
        {

            if(pubParams.OnlyOk) {
                pubs = pubs.Where(x => x.Aprobado == true);   
            }
            if (!string.IsNullOrEmpty(pubParams.Jornada))
            {
                pubs = pubs.Where(x => x.Jornada == pubParams.Jornada);
            }

            if(!string.IsNullOrEmpty(pubParams.Carrera)){
                pubs = pubs.Where(x => x.Carrera == pubParams.Carrera);
            }

            if (!string.IsNullOrEmpty(pubParams.Fecha))
            {
                DateTime dateNow = DateTime.Now;
                switch (pubParams.Fecha)
                {
                    case "Hoy":
                        pubs = pubs.Where(x => x.fecha.Date == dateNow.Date);
                        break;
                    case "Ayer":
                        pubs = pubs.Where(x => x.fecha.Date == dateNow.Date.AddDays(-1));
                        break;
                    case "Semana":
                        pubs = pubs.Where(x => x.fecha.Date > dateNow.Date.AddDays(-7));
                        break;
                    case "Mes":
                    pubs = pubs.Where(x => x.fecha.Date > dateNow.Date.AddMonths(-1));
                        break;
                    default:
                        break;
                }
            }


            
            // if(!string.IsNullOrEmpty(pubParams.Carrera)) {
            //     pubs = pubs.Where(x => x.Carerra == pubParams.Carrera);
            // }

            return pubs;
        }
    }
}