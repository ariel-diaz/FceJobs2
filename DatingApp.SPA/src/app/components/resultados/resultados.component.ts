import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Oferta } from '../../interfaces/oferta';
import { OfertasService } from '../../services/ofertas.service';
import { PaginatedResult } from '../../interfaces/pagination';
import { Pagination } from './../../interfaces/pagination';
import { Filter } from './../../interfaces/filter';

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.component.html',
  styles: []
})
export class ResultadosComponent implements OnInit {
  busqueda: string;
  resultados: Oferta[] = [];
  copyResultados: Oferta[] = [];
  pagination: Pagination;
  filter: Filter = new Filter();
  isLoad: boolean;

  constructor(private activatedRoute: ActivatedRoute,
    private _ofertaService: OfertasService) {

  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.busqueda = params['busqueda'];
      this.loadData();
    });
  }

  loadData() {
    this._ofertaService.getOfertas(true,this.busqueda).subscribe((resp: PaginatedResult<Oferta[]>) => {
      this.resultados = resp.result.filter(x => x.aprobado);
      this.pagination = resp.pagination;
      this.copyResultados = resp.result;
      this.isLoad = true;
    });
  }


  filterData(filter: string, value: string) {
    switch (filter) {
      case 'carrera':
        this.filter.carrera = value;
        break;
      case 'jornada':
        this.filter.jornada = value;
        break;
      case 'fecha':
        this.filter.fecha = value;
        break;
      default:
        console.log("Error")
    }
    this.loadItems();
  }


  pageChanged(event: any): void {
    this.pagination.currentPage = event.page;
    console.log(this.pagination.currentPage);
    this.loadItems();
  }

  loadItems() {
    this._ofertaService.getOfertas(true,this.busqueda, this.pagination.currentPage, this.pagination.itemsPerPage, this.filter)
      .subscribe((resp: PaginatedResult<Oferta[]>) => {
        this.resultados = resp.result.filter(x => x.aprobado);
        this.pagination = resp.pagination;
        this.isLoad = true;
      });
  }



}


