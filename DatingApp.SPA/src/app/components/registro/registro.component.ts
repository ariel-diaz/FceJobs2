import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styles: []
})
export class RegistroComponent implements OnInit {

  formRegister: FormGroup;
  constructor(public _authService: AuthService , public route: Router) { }

  ngOnInit() {

    this.formRegister = new FormGroup({
      username: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(4)]),
      password2: new FormControl('', [ Validators.required, Validators.minLength(4)])
    });

   }

   get f() {
     return this.formRegister.controls;
   }

  registarUsuario(user: FormGroup) {

    if (user.invalid) {
      console.log('El formulario es invalido!');
      return;
    }
    this._authService.registerUser(user.value).subscribe(resp => {
      console.log('Todo ok');
      this.route.navigate(['/login']);
    }, error => {
      console.log(error);
    });
  }
}
