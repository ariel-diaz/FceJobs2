import { Component, OnInit } from '@angular/core';
import { Oferta } from './../../interfaces/oferta';
import { OfertasService } from './../../services/ofertas.service';
import { MessagesService } from './../../services/messages.service';
import swal from 'sweetalert2';
import { PaginatedResult } from '../../interfaces/pagination';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styles: []
})
export class AdminComponent implements OnInit {

  publicaciones: Oferta[];
  busqueda: string;
  constructor(private _ofertaService: OfertasService,
    private _messageService: MessagesService) {
    this.cargarData(true);
  }

  ngOnInit() {
  }


  openDescription(descripcion: string) {
    this._messageService.modal(descripcion);
  }

  cargarData(flag: boolean) {
    if (flag) {
      this._ofertaService.getOfertas(false, null, 1, 50).subscribe((resp: PaginatedResult<Oferta[]>) => {
        this.publicaciones = resp.result.filter(resp => !resp.aprobado);
      });
    } else {
      this._ofertaService.getOfertas(false, null, 1, 50).subscribe((resp: PaginatedResult<Oferta[]>) => {
        this.publicaciones = resp.result.filter(resp => resp.aprobado);
      });
    }
  }

  aprobar(item: Oferta) {
    swal({
      title: item.aprobado ? 'Desaprobar publicaciòn?' : 'Aprobar publicación?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText:  item.aprobado ? 'Desaprobar!' : 'Aprobar!'
    }).then((result) => {
      if (result.value) {
        this._ofertaService.aprobar(+item.id).subscribe(resp => {;
        
          swal(
            item.aprobado ? 'Desaprobado' : 'Aprobado',
            '',
            'success'
          )
           this.publicaciones = this.publicaciones.filter(e => parseInt(e.id) !== +item.id);
        }, error => {
          console.log(error);
        });
      }
    });
  }




}
