import { Component, OnInit } from '@angular/core';
import { OfertasService } from './../../services/ofertas.service';
import { ActivatedRoute } from '@angular/router';
import { Oferta } from '../../interfaces/oferta';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styles: []
})
export class DetalleComponent implements OnInit {


  oferta: Oferta;
  user: string;
  constructor(public _ofertasService: OfertasService,
    public activatedRoute: ActivatedRoute) {
    this.user = localStorage.getItem('username');
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe(param => {
      const id = param['id'];
      if (id != null) {
        this._ofertasService.getOferta(id).subscribe((resp: Oferta) => {
          console.log(resp);
          this.oferta = resp;
        });
      }
    });

  }

  postular() {
    if (this.user === null) {
      Swal('Usuario nulo', '', 'error');
    } else if (this.user === this.oferta.user) {
      Swal('Error con el usuario!', 'No podes postularte a tu misma publicación!!!', 'error');
    } else {
      this._ofertasService.addPostulacion(this.oferta, this.user).subscribe(resp => {
        console.log(resp);
        Swal('Postulado!', '', 'success');
      }, error => {
        // Validar si esta postulado.
        console.log(error);
        Swal('Error!', '', 'error');
      });
    }
  }
}
