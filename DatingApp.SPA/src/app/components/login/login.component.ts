import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl} from '@angular/forms';
import { User } from '../../interfaces/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formLogin: FormGroup;

  constructor(public _authService: AuthService ,
     public route: Router) { }

  ngOnInit() {

    this.formLogin = new FormGroup({
      usuario: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });
  }

  loguearUsuario(form: FormGroup) {
    const user: User = {
      username:  form.value.usuario,
      password: form.value.password
    };

    this._authService.login(user).subscribe(resp => {
        this.route.navigate(['/home']);
    }, error => {
      console.log(error);
    });
  }


}
