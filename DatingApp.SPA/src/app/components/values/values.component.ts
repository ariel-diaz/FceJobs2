import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-values',
  templateUrl: './values.component.html',
  styleUrls: ['./values.component.css']
})
export class ValuesComponent implements OnInit {

  values: any;
  constructor(public _http: HttpClient, public _authService: AuthService) {
     this.getItems();
   }

  ngOnInit() {
  }

  getItems() {
    this._authService.getValues().subscribe(resp => {
      this.values = resp;
      console.log(resp);
    });
  }

}
