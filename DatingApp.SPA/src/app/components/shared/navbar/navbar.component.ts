import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent implements OnInit {

  constructor(public _authService: AuthService) {
   }

  ngOnInit() {
  }

  logout() {
    this._authService.logOut();
  }

  loggedIn() {
    return this._authService.isLoggedIn();
  }
}
