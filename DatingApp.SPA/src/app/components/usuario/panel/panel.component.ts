import { Component, OnInit } from '@angular/core';
import { Oferta } from '../../../interfaces/oferta';
import { AuthService } from '../../../services/auth.service';
import { UserService } from '../../../services/user.service';
import { OfertasService } from './../../../services/ofertas.service';
import { Postulacion } from './../../../interfaces/postulacion';
import { element } from 'protractor';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styles: []
})
export class PanelComponent implements OnInit {


  publicaciones: Oferta[];
  postulaciones: Postulacion[];
  user: string;
  constructor(private _userService: UserService,
    private _authService: AuthService,
    private _ofertasService: OfertasService) {

    this.user = this._authService.getUser();

  }

  ngOnInit() {
    this.getPublicaciones();
  }


  getPublicaciones() {
    this._userService.getPublicaciones(this.user).subscribe((resp: Oferta[]) => {
      this.publicaciones = resp;
      this.cargarPostulaciones();
    }, error => {
      console.log(error);
    });
  }

  cargarPostulaciones() {
    return this._ofertasService.getPostulaciones().subscribe((resp: Postulacion[]) => {
      this.postulaciones = resp;
      this.publicaciones.forEach(item => {
        item.postulaciones = this.postulaciones.filter(id => id.idPublicacion == item.id);
        
      });
    });
  };
}


