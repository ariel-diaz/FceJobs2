import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OfertasService } from './../../../services/ofertas.service';
import { Postulacion } from './../../../interfaces/postulacion';
import { UserService } from '../../../services/user.service';
import { User } from '../../../interfaces/user';
import { DomSanitizer } from '@angular/platform-browser';
import swal from 'sweetalert2';

@Component({
  selector: 'app-detalle-publicacion',
  templateUrl: './detalle-publicacion.component.html',
  styles: []
})
export class DetallePublicacionComponent implements OnInit {

  id: string;
  postus: Postulacion[];
  isLoad: boolean;
  constructor(private _activatedRoute: ActivatedRoute,
    private _ofertasService: OfertasService,
    private _userService: UserService, private _sanitizer: DomSanitizer) {

    this._activatedRoute.params.subscribe(param => {
      this.id = param['id'];
      this._ofertasService.getPostulacionesPub(this.id).subscribe((resp: Postulacion[]) => {
        this.postus = resp;
        this.postus.forEach(e => {
          this._userService.getUser(e.user).subscribe((x: User) => {
            e.username = x;
          });
        })
      });

      this.isLoad = true;
    });
  }

  getUrlSafe(url: string) {
    return this._sanitizer.bypassSecurityTrustUrl(url);
  }

  ngOnInit() {
  }

  rechazar(username: string) {
    swal({
      title: 'Rechazar?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si!'
    }).then((result) => {
      if (result.value) {
        this._ofertasService.deletePostulacion(+this.id, username).subscribe( () => {
           this.postus = this.postus.filter(x => x.user != username);
           if(this.postus.length == 0 ){
             window.location.reload();
           }
        }, error => {
          console.log(error);
        });
        
      };
    });
    
   
  }
}
