import { Component, OnInit } from '@angular/core';
import { HttpRequest, HttpClient, HttpEventType } from '@angular/common/http';
import { UserService } from './../../../services/user.service';
import { User } from '../../../interfaces/user';
import swal from 'sweetalert2';


const extensions = ['doc','docx','pdf'];
@Component({
  selector: 'app-uploadcv',
  templateUrl: './uploadcv.component.html',
  styles: []
})
export class UploadcvComponent implements OnInit {

   progress: number;
   message: string;
   user: User;
   pdfSrc: string;
  constructor(private http: HttpClient, private _userService: UserService) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._userService.getUser(localStorage.getItem('username')).subscribe((user: User) => {
      this.user = user;
      this.pdfSrc = user.cv.fileRoot;
      console.log(this.pdfSrc);
  });
  }

  upload(files) {
    if (files.length === 0) {
      return;
    }
    let extension = files[0].name.split('.').pop();
    if(!extensions.includes(extension)) {
      swal("Formato del archivo incorrecto!", "El CV tiene que estar en formato pdf, doc o docx.");
      return;
    }

    const formData = new FormData();
    var file = files[0];
    formData.append(file.name, file);
    var user = localStorage.getItem('username');

    const uploadReq = new HttpRequest('POST', `http://localhost:5000/api/upload?userName=${user}`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress)
        this.progress = Math.round(100 * event.loaded / event.total);
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();
        this.loadData();
        swal("Carga exitosa!");
    });
  }

}
