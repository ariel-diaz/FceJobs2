import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl} from '@angular/forms';
import { OfertasService } from '../../services/ofertas.service';
import { Oferta } from '../../interfaces/oferta';
import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
@Component({
  selector: 'app-nuevaoferta',
  templateUrl: './nuevaoferta.component.html',
  styleUrls: ['./nuevaoferta.component.css']
})
export class NuevaofertaComponent implements OnInit {

  formOferta: FormGroup;
  user: string;
  isLogged: boolean;

  constructor(public _ofertaService: OfertasService ,
              public route: Router,
              private _authService: AuthService
      ) {

        this.user = localStorage.getItem('username');
        this.isLogged = this._authService.isLoggedIn();
      }

  ngOnInit() {

    this.formOferta = new FormGroup({
      puesto: new FormControl(null, Validators.required),
        empresa: new FormControl(null, Validators.required ),
        localizacion: new FormControl(null, Validators.required ),
        salario: new FormControl(null, Validators.required ),
        jornada: new FormControl(null, Validators.required),
        descripcion: new FormControl(null, Validators.required),
        contacto: new FormControl(null, [Validators.required, Validators.email]),
        carrera: new FormControl(null, [Validators.required])
    });

  }


 registrarOferta(oferta: FormGroup) {

  console.log(oferta.value);
   const item: Oferta = ({
     id: '',
     puesto: oferta.value.puesto,
     salario: oferta.value.salario,
     user: this.user,
     localizacion: oferta.value.localizacion,
     descripcion: oferta.value.descripcion,
     jornada: oferta.value.jornada,
     empresa: oferta.value.empresa,
     contacto: oferta.value.contacto,
     fecha: new Date(Date.now()),
     postulaciones: [],
     aprobado: false,
     carrera: oferta.value.carrera
   });

   this._ofertaService.addOferta(item).subscribe(resp => {
     console.log(resp);
     this.route.navigate(['/resultados']);
   });

 }

}
