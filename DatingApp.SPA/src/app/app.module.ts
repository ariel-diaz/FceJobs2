import { AuthInterceptor } from './helpers/auth.interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent} from './components/shared/navbar/navbar.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { APP_ROUTING } from './routes';
import { ResultadosComponent } from './components/resultados/resultados.component';
import { RegistroComponent } from './components/registro/registro.component';
import { OfertasService } from './services/ofertas.service';
import { NuevaofertaComponent } from './components/nuevaoferta/nuevaoferta.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetalleComponent } from './components/detalle/detalle.component';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { LoginGuard } from './services/guards/login.guard';
import { PanelComponent } from './components/usuario/panel/panel.component';
import { ValuesComponent } from './components/values/values.component';
import { ErrorInterceptor } from './helpers/error.interceptor';
import { DetallePublicacionComponent } from './components/usuario/detalle-publicacion/detalle-publicacion.component';
import { AdminComponent } from './components/admin/admin.component';
import { MessagesService } from './services/messages.service';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { DateDifferencePipe } from './pipes/date-difference.pipe';
import { UploadcvComponent } from './components/usuario/uploadcv/uploadcv.component';
import { SatitizePipe } from './pipes/satitize.pipe';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    PageNotFoundComponent,
    ResultadosComponent,
    RegistroComponent,
    NuevaofertaComponent,
    DetalleComponent,
    LoginComponent,
    PanelComponent,
    ValuesComponent,
    DetallePublicacionComponent,
    AdminComponent,
    DateDifferencePipe,
    UploadcvComponent,
    SatitizePipe
  ],
  imports: [
  BrowserModule,
    APP_ROUTING,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    PaginationModule.forRoot(),
    PdfViewerModule
  ],
  providers: [OfertasService, AuthService, LoginGuard, UserService, MessagesService,
     {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
     {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}
    ] ,
  bootstrap: [AppComponent]
})
export class AppModule { }
