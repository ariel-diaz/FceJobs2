import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { User } from '../interfaces/user';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class UserService {

   url = 'http://localhost:5000/api/';
   userSource: BehaviorSubject<User> = new BehaviorSubject<User>(new User());
   currentUser = this.userSource.asObservable();
  constructor(private _http: HttpClient) { }

  setCurentUser(user: User) {
    this.userSource.next(user);
  }

  cleanCurrentUser() {
    this.userSource.next(new User());
  }

  getPublicaciones(user: string) {
    const url = this.url + 'publicaciones/panel/' + user;
    return this._http.get(url, this.requestOptions());
   }

   getUser(user: string) {
     const url = this.url + 'user/' + user;
     return this._http.get(url);
   }

   private requestOptions() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
      return httpOptions;
  }
}
