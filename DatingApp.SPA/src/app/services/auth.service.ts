import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders  } from '@angular/common/http';
import { User } from '../interfaces/user';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url = 'http://localhost:5000/api/auth/';
  userToken: string;
  userName: string;
  role: string;

  constructor(public _http: HttpClient,
     public route: Router,
    private _userService: UserService) {
    this.loadStorage();
  }


  loadStorage() {
    if (localStorage.getItem('token'))  {
      this.userToken = localStorage.getItem('token');
      this.userName = localStorage.getItem('username');
      this.role = localStorage.getItem('result');
    } else {
      this.userToken = '';
    }
  }

  isLoggedIn() {
    this.loadStorage();
    return ( this.userToken.length > 5 ) ? true : false;
  }

  logOut() {
    this.userToken = null;
    this.userName = '';
    localStorage.removeItem('token');
    localStorage.removeItem('username');
    localStorage.removeItem('result');
    this.route.navigate(['/home']);
    this._userService.cleanCurrentUser();
  }

  get username() {
    return this.userName;
  }

  getUser() {
    return this.userName;
  }

  login(user: User) {
    const url = this.url + 'login';
    return  this._http.post(url, user, this.requestOptions()).pipe(map((resp: Response) => {
        // tslint:disable-next-line:no-shadowed-variable
        const user: any = resp;
        if (user) {
          console.log(user);
          localStorage.setItem('token', user.tokenString);
          localStorage.setItem('username', user.username);
          localStorage.setItem('result', user.result);
          this.userName = user.username;
          this.userToken = user.tokenString;
          this.role = user.role;
          this._userService.setCurentUser(user);
        }
    }), catchError( error => this.handleError(error))
);
  }

  registerUser(user: User) {
    const url = this.url + 'register';
    return this._http.post(url, user, this.requestOptions()).pipe(catchError( error => this.handleError(error)));
  }

getValues() {
  return this._http.get('http://localhost:5000/api/values', this.requestOptions());
}


isAdmin() {
  if(this.role === "ADMIN") return true;
  return false
}


 private handleError(error: any) {
   const applicationError = error.headers.get('Application-Error');
   if ( applicationError ) {
     return throwError(applicationError);
   }
   const serverError = error;
   let modelStateErrors = '';
   if ( serverError ) {
      for ( const key in serverError) {
        if ( serverError[key]) {
          modelStateErrors += serverError[key] + '\n';
        }
      }
   }
   return throwError(
     modelStateErrors || 'Server Error'
   );
 }

  private requestOptions() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
      return httpOptions;
  }
}
