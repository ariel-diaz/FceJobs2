import { AuthService } from './../auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor( public _authService: AuthService ,
   public route: Router) {
  }

  canActivate() {
    if (this._authService.isLoggedIn()) {
      return true;
    } else {
      this.route.navigate(['/login']);
    }
  }

}
