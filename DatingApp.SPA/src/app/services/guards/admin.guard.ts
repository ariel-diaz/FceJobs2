import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {


  constructor(private route: Router) {
   
  }
  canActivate() {
    let role = localStorage.getItem('result');
    console.log(role);
    if(role === "ADMIN") {
      return true;
    } else {
      this.route.navigate(['/login']);
      console.log("NO TIENE ACCESO");
    }

  }
    
}
