import { Injectable } from '@angular/core';
import { Oferta } from '../interfaces/oferta';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Postulacion } from '../interfaces/postulacion';
import { PaginatedResult } from '../interfaces/pagination';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/internal/operators/catchError';
import { Filter } from '../interfaces/filter';
import { User } from './../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class OfertasService {

  ofertas: Oferta[] = [];
  url = 'http://localhost:5000/api/';

  constructor(public _http: HttpClient) {
  }


  addOferta(oferta: Oferta) {
    oferta.id = this.ofertas.length.toString();
    const url = this.url + 'publicaciones';
    return this._http.post(url, oferta);
  }

  aprobar(id: number) {
    const url = this.url + 'publicaciones/' + id;
    return this._http.put(url, id, this.requestOptions());
  }

  getOfertas(onlyOk: boolean,busqueda?: string, page?, itemsPerPage?, filterPub?: Filter): Observable<PaginatedResult<Oferta[]>> {
    console.log('La busqueda es: ' + busqueda);
    const apiUrl = busqueda !== null && busqueda !== '' && busqueda !== undefined ? ('busqueda/' + busqueda) : 'publicaciones';
    const url =  this.url + apiUrl;
    const paginatedResult: PaginatedResult<Oferta[]> = new PaginatedResult<Oferta[]>();
    let params = new HttpParams();
    if (page !== null && itemsPerPage !== null) {
      params = params.append('pageNumber', page);
      params = params.append('itemsPerPage', itemsPerPage);
    }

    if(filterPub != null) {
      params = filterPub.jornada ? params.append('jornada', filterPub.jornada) : params;
      params = filterPub.carrera ? params.append('carrera', filterPub.carrera) : params;
      params = filterPub.fecha ? params.append('fecha', filterPub.fecha) : params;
    }
    if(onlyOk) {
      params = params.append('onlyOk', onlyOk.toString());
    }


    return this._http.get<Oferta[]>(url, { observe: 'response', params })
      .pipe(
        map(resp => {
          paginatedResult.result = resp.body;
          if (resp.headers.get('pagination') != null) {
            paginatedResult.pagination = JSON.parse(resp.headers.get('pagination'));
          }
          return paginatedResult;
        })
      );
  }


  getOferta(id: string) {
    const url = this.url + 'publicaciones/' + id;
    return this._http.get(url);
  }

  addPostulacion(oferta: Oferta, user: string) {
    const url = this.url + 'postulaciones/';

    const postulacion: Postulacion = {
      user: user,
      idPublicacion: oferta.id,
      status: false
    };
    return this._http.post(url, postulacion, this.requestOptions()).pipe(
      catchError( (err, caught) => {
        return err;
      })
    );
  }


  getPostulaciones() {
    const url = this.url + 'postulaciones';
    return this._http.get(url);
  }

  getPostulacionesPub(id: string) {
    const url = this.url + 'postulaciones/' + id;
    return this._http.get(url);
  }n

  deletePostulacion(idOferta: number, user: string) {
    const url = this.url + 'postulaciones?idOferta=' + idOferta + '&user=' + user;
    return this._http.delete(url);
  }


  private requestOptions() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return httpOptions;
  }
}


