import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateDifference'
})
export class DateDifferencePipe implements PipeTransform {

  transform(value: any, args?: any): any {

    var dateNow = new Date(Date.now());
    var newDate = new Date(value);

   var diff = dateNow.getTime() - newDate.getTime();
   var resDiff = Math.round(diff/(1000*60*60*24));
   var res = resDiff === 0 ? 'Publicado Hoy' : `Publicado hace ${ Math.round(diff/(1000*60*60*24))} días`;
   return res;
 
  }

}
