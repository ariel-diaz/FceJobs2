import { User } from './user';
export interface Postulacion {
    user: string;
    idPublicacion: string;
    status: boolean;
    username?: User;
}
