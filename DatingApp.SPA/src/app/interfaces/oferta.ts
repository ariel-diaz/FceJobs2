import { Postulacion } from "./postulacion";
export interface Oferta {
id: string;
user: string;
   puesto: string;
   salario: string;
   localizacion: string;
   descripcion: string;
   jornada: string;
   empresa: string;
   contacto: string;
   fecha: Date;
   postulaciones: Postulacion[];
   aprobado: boolean;
   carrera: string;
}
