import { Cv } from "./cv";

export class User {
    username: string;
    email?: string;
    password: string;
    role?: string;
    cv?: Cv;
}
