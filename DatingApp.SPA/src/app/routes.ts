import { LoginGuard } from './services/guards/login.guard';
import { LoginComponent } from './components/login/login.component';
import { DetalleComponent } from './components/detalle/detalle.component';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ResultadosComponent } from './components/resultados/resultados.component';
import { RegistroComponent } from './components/registro/registro.component';
import { NuevaofertaComponent } from './components/nuevaoferta/nuevaoferta.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { PanelComponent } from './components/usuario/panel/panel.component';
import { ValuesComponent } from './components/values/values.component';
import { DetallePublicacionComponent } from './components/usuario/detalle-publicacion/detalle-publicacion.component';
import { AdminComponent } from './components/admin/admin.component';
import { AdminGuard } from './services/guards/admin.guard';
import { UploadcvComponent } from './components/usuario/uploadcv/uploadcv.component';

const appRoutes: Routes =
    [
        { path: 'home', component: HomeComponent },
        { path: 'resultados', component: ResultadosComponent },
        { path: 'resultados/:busqueda', component: ResultadosComponent },
        { path: 'nuevaoferta', component: NuevaofertaComponent, canActivate: [LoginGuard] },
        { path: 'registro', component: RegistroComponent },
        { path: 'detalle/:id', component: DetalleComponent },
        { path: 'login', component: LoginComponent },
        { path: 'values', component: ValuesComponent },
        { path: 'admin', component: AdminComponent, canActivate: [LoginGuard, AdminGuard] },
        { path: 'tucv', component: UploadcvComponent, canActivate: [LoginGuard] },
        {
            path: 'panel', 
            component: PanelComponent,
            canActivate: [LoginGuard],
            children: [
                 {
                     path: ':id', component: DetallePublicacionComponent }
                  ]
        },
        { path: '', redirectTo: '/home', pathMatch: 'full' },
        { path: '**', component: PageNotFoundComponent }
    ];


export const APP_ROUTING = RouterModule.forRoot(appRoutes);



